import { Component, ViewChild } from '@angular/core';

import { Platform, MenuController, NavController, ToastController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router, RouterEvent } from '@angular/router';
import { async } from '@angular/core/testing';
import { AppRoutingModule } from './app-routing.module';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {

  @ViewChild(AppRoutingModule, {static: false}) routerOutlet: AppRoutingModule;

  public pages = [
    {
      title: "Tela Inicial",
      url: "./home",
      icon: "home"
    },
    {
      title: "Relatório",
      url: "./relatorios",
      icon: "clipboard"
    }
  ];

  public selectPath = '';

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private menu: MenuController,
    private router: Router,
    private navCtrl: NavController,
    private toastController: ToastController
  ) {
    this.initializeApp();
    this.router.events.subscribe((event: RouterEvent) => {
      this.selectPath = event.url;
    })
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.statusBar.backgroundColorByHexString("#fff");
      this.splashScreen.hide();

      this.platform.backButton.subscribeWithPriority(0, async() => {
        
      })
    });

  }

  // openFirst() {
  //   this.menu.enable(true, 'first');
  //   this.menu.open('first');
  // }

  // openEnd() {
  //   this.menu.open('end');
  // }

  // openCustom() {
  //   this.menu.enable(true, 'custom');
  //   this.menu.open('custom');
  // }
}
