import { Component, OnInit } from '@angular/core';
import { MenuController, NavController } from '@ionic/angular';

@Component({
  selector: 'app-home-aluno',
  templateUrl: './home-aluno.page.html',
  styleUrls: ['./home-aluno.page.scss'],
})

export class HomeAlunoPage implements OnInit {

  constructor(
    private menuCtrl: MenuController,
    public navCtrl: NavController
  ){

  }

  ngOnInit(){
    this.menuCtrl.enable(false, 'custom')
  }

  telaAvDocente(){
    this.navCtrl.navigateRoot('avaliacao-docente')
  }

}
