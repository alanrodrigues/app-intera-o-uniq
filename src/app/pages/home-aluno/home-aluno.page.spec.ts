import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HomeAlunoPage } from './home-aluno.page';

describe('HomeAlunoPage', () => {
  let component: HomeAlunoPage;
  let fixture: ComponentFixture<HomeAlunoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeAlunoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HomeAlunoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
