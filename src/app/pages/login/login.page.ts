import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/interfaces/user';
import { AutenticacaoService } from 'src/app/services/autenticacao.service';
import { LoadingController, ToastController } from '@ionic/angular';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { AlunoService } from 'src/app/services/aluno.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  private loading: any;
  public userLogin: User = {};
  private alunoSubscription: Subscription;

  constructor(
    private authService: AutenticacaoService,
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    public keyboard: Keyboard,
    private alunoService: AlunoService,
    private router: Router,
    private storage: Storage

  ) { }

  ngOnInit() {
  }

  // converterParaData(timestamp){
  //   var theDate = new Date(timestamp * 1000);
  //   var data = theDate.toGMTString();

  //   return data;
  // }

  async login() {
    await this.presentLoading();

    try{
      this.alunoSubscription = this.alunoService.getAlunos().subscribe(data => {
        for(let i = 0; i < data.length; i++){
          
          if(data[i].email == this.userLogin.email && this.userLogin.password == data[i].cpf){
            this.storage.set('cursoSelecionado', data[i]);
            this.router.navigate(['home-aluno']);
          }else{
            this.presentToast('Usuário não encontrado');
            this.router.navigate(['login']);
          }
        }
       });
    }catch(error){
      console.log(error);
    }finally {
        this.loading.dismiss()
      }



    try {
      await this.authService.login(this.userLogin);
    } catch (error) {
      let message: string;
      switch (error.code) {
        case 'auth/wrong-password':
          message = 'Senha incorreta!';
          break;
        case 'auth/user-not-found':
        //  message = 'Usuário não encontrado!';
          break;
        case 'auth/invalid-email':
          message = 'E-mail inválido!';
          break;
        case 'auth/argument-error':
          message = 'Por favor preencha todos os campos!';
          break;
      }
      this.presentToast(message);
    } finally {
      this.loading.dismiss()
    }

  }

  async presentLoading() {
    this.loading = await this.loadingCtrl.create({ message: 'Aguarde, por favor' });
    return this.loading.present();
  }

  async presentToast(message: string) {
    const toast = await this.toastCtrl.create({ message, duration: 2000 });
    toast.present();
  }

}
