import { Component, OnInit } from '@angular/core';
import { GoogleChartInterface } from 'ng2-google-charts/google-charts-interfaces';
import { AutenticacaoService } from 'src/app/services/autenticacao.service';
import { AlertController, MenuController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { AvaliacaoService } from 'src/app/services/avaliacao.service';

@Component({
  selector: 'app-relatorios',
  templateUrl: './relatorios.page.html',
  styleUrls: ['./relatorios.page.scss'],
})
export class RelatoriosPage implements OnInit {
  private avaliacaoSubscription: Subscription;
  private dados;
  positivo;
  public pieChart: GoogleChartInterface;

  constructor(
    private autenticacaoService: AutenticacaoService,
    public alertController: AlertController,
    private menuCtrl: MenuController,
    private avaliacaoServices: AvaliacaoService
  ) { 
    this.avaliacaoSubscription = this.avaliacaoServices.getAvaliacoes().subscribe(data => {
      this.dados = data;
      
      for(let dado of this.dados){
        console.log(this.dados, dado.avaliou)
     
        for(let i = 0; i < dado.avaliou; i++){
          if(i == 1){
            console.log("Foi")
          }
        }
      }
    })
  }

  ngOnInit() {
    this.loadSimplePieChart();
  }

  openCustom() {
   
    this.menuCtrl.open('custom');
   }

  loadSimplePieChart() {
    this.pieChart = {
      chartType: 'PieChart',
      dataTable: [
        ['', ''],
        ['Gostaram', 2],
        ['Não Gostaram', 1]
      ],
      //opt_firstRowIsData: true,
      options: {
        height: 200,
        width: '80%',
        is3D: true,
        colors: ['#00FF00', '#FF0000']
      },
    };
  }

  async sair() {
    const alert = await this.alertController.create({
      header: 'Atenção!',
      message: 'Deseja realmente sair?',
      buttons: [
        {
          text: 'Não',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Sim',
          handler: async () => {
            await this.autenticacaoService.logout();
          }
        }
      ]
    });

    await alert.present();
  }


}
