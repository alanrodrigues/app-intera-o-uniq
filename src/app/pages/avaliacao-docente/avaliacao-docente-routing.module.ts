import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AvaliacaoDocentePage } from './avaliacao-docente.page';

const routes: Routes = [
  {
    path: '',
    component: AvaliacaoDocentePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AvaliacaoDocentePageRoutingModule {}
