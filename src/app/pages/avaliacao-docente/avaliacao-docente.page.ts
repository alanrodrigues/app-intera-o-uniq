import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Turmas } from 'src/app/interfaces/turmas';
import { AutenticacaoService } from 'src/app/services/autenticacao.service';
import { AlertController, MenuController, ToastController, LoadingController } from '@ionic/angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { TurmaService } from 'src/app/services/turma.service';
import { AvaliacaoService } from 'src/app/services/avaliacao.service';
import { CursosService } from 'src/app/services/cursos.service';
import { Curso } from 'src/app/interfaces/curso';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';

@Component({
  selector: 'app-avaliacao-docente',
  templateUrl: './avaliacao-docente.page.html',
  styleUrls: ['./avaliacao-docente.page.scss'],
})
export class AvaliacaoDocentePage implements OnInit {

  codeEscaneado = null;
  private loading: any;
  // private turmasSubscription: Subscription;
  // public turmas = new Array<Turmas>();
  private cursosSubscription: Subscription;
  private usuario;
  private curso;
  private dadosDisciplina;

  constructor(
    private autenticacaoService: AutenticacaoService,
    public alertController: AlertController,
    private barcodeScanner: BarcodeScanner,
    private menuCtrl: MenuController,
    public toastCtrl: ToastController,
    private cursosService: CursosService,
    private loadingCtrl: LoadingController,
    private avaliacaoService: AvaliacaoService,
    private storage: Storage,
    private router: Router,
  ) {


  }

  ngOnInit() {
    this.menuCtrl.enable(false, 'custom')

    this.storage.get('cursoSelecionado').then((val) => {
      this.usuario = val;
    });
  }


  ngOnDestroy() {
    //this.cursosSubscription.unsubscribe();
  }

  async sair() {
    const alert = await this.alertController.create({
      header: 'Atenção!',
      message: 'Deseja realmente sair?',
      buttons: [
        {
          text: 'Não',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Sim',
          handler: async () => {
            await this.router.navigate(['login'])
            this.storage.clear();
          }
        }
      ]
    });

    await alert.present();
  }

  async gostou() {
    await this.presentLoading();
    let data = {
      avaliou: 1,
      positiva: 1,
      negativa: 0,
      userId: this.usuario.cpf,
      professor: this.dadosDisciplina.professor,
      disciplina: this.dadosDisciplina.disciplina,
      curso: this.curso.nomeTurma
    }


    try {
      await this.avaliacaoService.adicionarAvaliacao(data);
      await this.loading.dismiss();
      await this.router.navigate(['login'])
      this.presentToast("Salvo com sucesso");
    } catch (error) {
      this.presentToast("Erro ao tentar salvar");
      console.log(error)
      this.loading.dismiss();
    }
  }

  async deslike() {
    await this.presentLoading();
    let data = {
      avaliou: 1,
      positiva: 0,
      negativa: 1,
      userId: this.usuario.cpf,
      professor: this.dadosDisciplina.professor,
      disciplina: this.dadosDisciplina.disciplina,
      curso: this.curso.nomeTurma
    }


    try {
      await this.avaliacaoService.adicionarAvaliacao(data);
      await this.loading.dismiss();
      await this.router.navigate(['login'])
      this.presentToast("Salvo com sucesso");
    } catch (error) {
      this.presentToast("Erro ao tentar salvar");
      console.log(error)
      this.loading.dismiss();
    }
  }

  compararHora(hora1, hora2, hora3) {
    hora1 = hora1.split(":");
    hora2 = hora2.split(":");
    hora3 = hora3.split(":");
    var d = new Date();
    var data1 = new Date(d.getFullYear(), d.getMonth(), d.getDate(), hora1[0], hora1[1]);
    var data2 = new Date(d.getFullYear(), d.getMonth(), d.getDate(), hora2[0], hora2[1]);
    var data3 = new Date(d.getFullYear(), d.getMonth(), d.getDate(), hora3[0], hora3[1]);


    return data1 >= data2 && data1 <= data3;
  };

  async scanCode() {

    try {
      this.cursosSubscription = await this.cursosService.getCursos().subscribe(async data => {

        //console.log(data)

        var dat = new Date();
        var hora = dat.getHours();
        var min = dat.getMinutes();
        var str_hora = hora + ':' + min;
        //this.converterParaData(data[0].inicioSemestre)
        for (let i = 0; i < data.length; i++) {
          for (let j = 0; j < data[i].dadosDisciplina.length; j++) {
            let horaInicio = data[i].dadosDisciplina[j].horaInicio;
            let horaFim = data[i].dadosDisciplina[j].horaFim;

            var resultado = this.compararHora(str_hora, horaInicio, horaFim);



            if (resultado) {
              if (data[i].id == this.usuario.cursoSelecionado) {
                this.barcodeScanner.scan().then(barCodeData => {
                  this.codeEscaneado = barCodeData.text;

                });
                this.dadosDisciplina = await data[i].dadosDisciplina[j]
                this.curso = await data[i];
                console.log(this.dadosDisciplina, resultado);
              }
            } else {
              if (this.dadosDisciplina) {
                this.presentToast('Você não está no horário de aula')
              }
            }



          }
        }

      })
    } catch (erro) {
      console.log(erro)
    }


    //this.presentAlert()
  }

  // converterParaData(timestamp) {
  //   var theDate = new Date(timestamp.seconds * 1000);
  //   var data = theDate.toGMTString();



  //   return data;
  // }

  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Atenção',
      message: 'Horário de avaliação não permitido!',
      buttons: ['OK']
    });

    await alert.present();
  }

  async presentLoading() {
    this.loading = await this.loadingCtrl.create({ message: 'Aguarde...' });
    return this.loading.present();
  }

  async presentToast(message: string) {
    const toast = await this.toastCtrl.create({ message, duration: 2000 });
    toast.present();
  }


}
