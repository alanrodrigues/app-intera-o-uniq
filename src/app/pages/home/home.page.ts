import { Component, OnInit } from '@angular/core';
import { AutenticacaoService } from 'src/app/services/autenticacao.service';
import { AlertController, ToastController, MenuController, LoadingController } from '@ionic/angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { Base64ToGallery } from '@ionic-native/base64-to-gallery/ngx';
import { Turmas } from 'src/app/interfaces/turmas';
import { DatePicker } from '@ionic-native/date-picker/ngx';
import { GoogleChartInterface } from 'ng2-google-charts/google-charts-interfaces';
import { TurmaService } from 'src/app/services/turma.service';



@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  qrData = null;
  codeEscaneado = null;
  codeCriado = null;
  public dadosTurma: Turmas = {};
  private loading: any;
  public dataSemestreInicio;
  public dataSemestreFim;
  public horaInicio;
  public horaFim;

  elementType: 'canvas' = 'canvas';

  constructor(
    private autenticacaoService: AutenticacaoService,
    public alertController: AlertController,
    private turmaService: TurmaService,
    public toastCtrl: ToastController,
    private barcodeScanner: BarcodeScanner,
    private base64ToGallery: Base64ToGallery,
    private loadingCtrl: LoadingController,
    private datePicker: DatePicker,
    private menuCtrl: MenuController
  ) { }

  ngOnInit() {
    this.menuCtrl.enable(true, 'custom');
  }

   openCustom() {
   
   this.menuCtrl.open('custom');
  }

  async sair() {
    const alert = await this.alertController.create({
      header: 'Atenção!',
      message: 'Deseja realmente sair?',
      buttons: [
        {
          text: 'Não',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Sim',
          handler: async () => {
            await this.autenticacaoService.logout();
          }
        }
      ]
    });

    await alert.present();
  }

  calendarioInicio(){
  this.datePicker.show({
    date: new Date(),
    mode: 'date',
    androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT,
  }).then(async (date) => {
      let dia: any;
      dia = date.getDate() > 10 ? date.getDate() : '0' + date.getDate();
      this.dataSemestreInicio = dia + '/0' + (date.getMonth() + 1) + '/' + date.getFullYear();
      console.log(this.dataSemestreInicio);
    }).catch((err) =>{
      console.log(err);
    });
  }

  calendarioFim(){
    this.datePicker.show({
      date: new Date(),
      mode: 'date',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT,
    }).then(async (date) => {
        let dia: any;
        dia = date.getDate() > 10 ? date.getDate() : '0' + date.getDate();
        this.dataSemestreFim = dia + '/0' + (date.getMonth() + 1) + '/' + date.getFullYear();
        console.log(this.dataSemestreFim);
      }).catch((err) =>{
        console.log(err);
      });
    }

    relogioInicio(){
      this.datePicker.show({
        date: new Date(),
        mode: 'time',
        androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_DARK,
      }).then(
        time => {
          this.horaInicio =  time.getHours()+":"+time.getMinutes();
        },
        err => console.log('Error occurred while getting time: ', err)
      );
    } 
    
    relogioFim(){
      this.datePicker.show({
        date: new Date(),
        mode: 'time',
        androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_DARK,
      }).then(
        time => {
          this.horaFim =  time.getHours()+":"+time.getMinutes();
        },
        err => console.log('Error occurred while getting time: ', err)
      );
    }  


  async salvar() {
    await this.presentLoading();
    let data = {
      codeQr: this.qrData,
      nomeTurma: this.dadosTurma.nome,
      inicioSemestre: this.dataSemestreInicio,
      fimSemestre: this.dataSemestreFim,
      inicioHorario: this.horaInicio,
      fimHorario: this.horaFim,
      sala: this.dadosTurma.sala,
      userId: this.autenticacaoService.getAuth().currentUser.uid
    }


    try{
      await this.turmaService.adicionarTurmas(data);
      await this.loading.dismiss();

      this.presentToast("Salvo com sucesso");
    }catch(error){
      this.presentToast("Erro ao tentar salvar");
      console.log(error)
      this.loading.dismiss();
    }
  }

  downloadQR() {
    const canvas = document.querySelector('canvas') as HTMLCanvasElement;
    const imageData = canvas.toDataURL('image/jpeg').toString();
    console.log('data: ', imageData);

    let data = imageData.split(',')[1];

    this.base64ToGallery.base64ToGallery(data,
      { prefix: '_img', mediaScanner: true })
      .then(async res => {
        let toast = await this.toastCtrl.create({
          header: 'Salvando imagem'
        });
        toast.present();
      }, err => console.log('Erro salver ', err));
  }

  async presentLoading() {
    this.loading = await this.loadingCtrl.create({ message: 'Aguarde...' });
    return this.loading.present();
  }

  async presentToast(message: string) {
    const toast = await this.toastCtrl.create({ message, duration: 2000 });
    toast.present();
  }

}
