import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AutenticacaoService } from '../services/autenticacao.service';

@Injectable({
  providedIn: 'root'
})
export class LogadoGuard implements CanActivate {


  constructor(
    private autenticacaoService: AutenticacaoService,
    private router: Router
  ) { }

  canActivate(): Promise<boolean> {
    return new Promise(resolve => {
      this.autenticacaoService.getAuth().onAuthStateChanged(user => {
        if (user != null) {
          if (user.uid == "6gv6JXaZR2bBktshcUkeCF5urcf2") {
            if (user) this.router.navigate(['home']);
            resolve(!user ? true : false);
          }
          // } else if (user.uid == 'lSiQ1V8bhSYKtHLl8SgHylknbol1') {
              
          //   if (user) this.router.navigate(['home-aluno']);
            
          //   resolve(!user ? true : false);
          // }
        }
        resolve(!user ? true : false);
      });
    });
  }

}
