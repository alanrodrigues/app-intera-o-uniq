import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AutenticacaoGuard } from './guards/autenticacao.guard';
import { LogadoGuard } from './guards/logado.guard';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then(m => m.LoginPageModule),
    canActivate: [LogadoGuard]
  },
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then(m => m.HomePageModule),
    canActivate: [AutenticacaoGuard]
  },
  {
    path: 'home-aluno',
    loadChildren: () => import('./pages/home-aluno/home-aluno.module').then(m => m.HomeAlunoPageModule),
    
  },
  {
    path: 'relatorios',
    loadChildren: () => import('./pages/relatorios/relatorios.module').then( m => m.RelatoriosPageModule),
    canActivate: [AutenticacaoGuard]
  },
  {
    path: 'avaliacao-docente',
    loadChildren: () => import('./pages/avaliacao-docente/avaliacao-docente.module').then( m => m.AvaliacaoDocentePageModule),
    
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
