import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { Curso } from '../interfaces/curso';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CursosService {

  private cursoCollection: AngularFirestoreCollection<Curso>;

  constructor(private afs: AngularFirestore) {
    this.cursoCollection = this.afs.collection<Curso>('Cursos');
  }

  getCursos() {
    return this.cursoCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;

          return { id, ...data };
        });
      })
    );
  }
}
