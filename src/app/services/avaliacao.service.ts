import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Avaliacao } from '../interfaces/avaliacao';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AvaliacaoService {
  private avaliacaoColecao: AngularFirestoreCollection<Avaliacao>;

  constructor(private afs: AngularFirestore) { 
    this.avaliacaoColecao = this.afs.collection<Avaliacao>('Avaliacao');
  }

  adicionarAvaliacao(avaliacao: Avaliacao){
    return this.avaliacaoColecao.add(avaliacao);
  }

  getAvaliacoes() {
    return this.avaliacaoColecao.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
         
          return { id, ...data };
        });
      })
    );
  }

  
}
