import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { Turmas } from '../interfaces/turmas';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TurmaService {

  private turmasColecao: AngularFirestoreCollection<Turmas>;

  constructor(private afs: AngularFirestore) { 
    this.turmasColecao = this.afs.collection<Turmas>('Turmas');
  }

  getTurmas(){
    return this.turmasColecao.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.data();
          return { id, ...data };
        })
      })
    )
  }

  adicionarTurmas(turmas: Turmas){
    return this.turmasColecao.add(turmas);
  }

}
