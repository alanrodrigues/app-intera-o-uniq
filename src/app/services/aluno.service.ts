import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { Aluno } from '../interfaces/aluno';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AlunoService {

  private alunoCollection: AngularFirestoreCollection<Aluno>;

  constructor(private afs: AngularFirestore) {
    this.alunoCollection = this.afs.collection<Aluno>('Alunos');
  }

  getAlunos() {
    return this.alunoCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;

          return { id, ...data };
        });
      })
    );
  }
}
