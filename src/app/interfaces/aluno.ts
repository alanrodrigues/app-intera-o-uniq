export class Aluno{
    codeQr?: string;
    nome?: string;
    email?: string;
    cpf?: string; 
    cursoSelecionado?: string;
    turnoSelecionado?: string;
}