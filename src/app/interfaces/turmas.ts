export interface Turmas {
    sala?: string,
    nome?: string,
    codeQr?: string,
    idAluno?: number,
    idProfessor?: number,
    inicioSemestre?: string,
    fimSemestre?: string,
    inicioHorario?: string,
    fimHorario?: string,
    userId?: string;
}