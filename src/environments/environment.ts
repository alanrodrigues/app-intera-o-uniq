// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyD9OL-lTv27lGnqnsXPT5kGvQ8YUVjy86g",
    authDomain: "uniq-f2044.firebaseapp.com",
    databaseURL: "https://uniq-f2044.firebaseio.com",
    projectId: "uniq-f2044",
    storageBucket: "uniq-f2044.appspot.com",
    messagingSenderId: "546141843351",
    appId: "1:546141843351:web:995a21efdabd4d1e64ded2",
    measurementId: "G-8SYDZX07S6"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
